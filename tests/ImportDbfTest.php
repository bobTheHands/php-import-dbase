<?php

namespace Tests\vdeApps\Import\ImportDbf;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Error\Error;
use vdeApps\Import\ImportAbstract;
use vdeApps\Import\ImportDbf;

class ImportDbfTest extends TestCase
{
    private $conn = null;
    
    public function createDb()
    {
        // $user = 'vdeapps';
        // $password = 'vdeapps';
        // $path = __DIR__.'/files/database.db';
        // $memory = false;
        $driverClass = '';
        $host = 'localhost';
        $port = '3306';
        $user = 'root';
        $password = 'bitnami';
        $dbname = 'test';
        
        $config = new \Doctrine\DBAL\Configuration();
        try {
            $connectionParams = [
                'driver' => 'pdo_mysql',
                'host' => $host,
                'port' => $port,
                'user' => $user,
                'password' => $password,
                'dbname' => $dbname,
                'memory' => false
            ];
            $this->conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
        } catch (\Exception $ex) {
            $this->conn = false;
            throw $ex;
        }
        
        return $this->conn;
    }
    
    public function _testImport1()
    {
        $this->createDb();
    
    
        $charsetDbf = 'cp863';
        $charsetDb = 'utf8_general_ci';
    
        $localFilename = __DIR__ . '/files/Agentes.DBF';
        $tablename = 'dbf_import_agentes';
        $imp = new ImportDbf($this->conn);
    
        $imp
            ->fromFile($localFilename)
            ->setDbfCharset($charsetDbf)
            ->setCharset($charsetDb)
            //                ->setLimit(10)
            // Destination table
            ->setTable($tablename)
            //Ignore la premiere ligne
            ->setIgnoreFirstLine(false)
            // Prend la première ligne comme entête de colonnes
            ->setHeaderLikeFirstLine(true)
            // Colonnes personnalisées
            //                            ->setFields($customFields)
            // Ajout de champs supplémentaires
                            ->addFields(['calc_iduser', 'calc_ident'])
            // Ajout de n colonnes
//            ->addFields(10)
            // Ajout d'un plugins
            ->addPlugins([$imp, 'pluginsNullValue'])
            // Ajout d'un plugins
            //                ->addPlugins(function ($rowData) {
            //                    $rowData['calcIduser'] = 'from plugins:' . $rowData['pkChantier'];
            //                    $rowData['calcIdent'] = 'from plugins:' . $rowData['uri'];
            //
            //                    return $rowData;
            //                })
            // required: Lecture/vérification
            ->read()
            // Exec import
            ->import();
    
        $this->assertEquals(1960, count($imp->getRows()));
    }
    
    public function testImport2()
    {
        $this->createDb();
        
        
        $charsetDbf = 'cp863';
        $charsetDb = 'utf8_general_ci';
        
        $localFilename = __DIR__ . '/files/Agentes.DBF';
        $tablename = 'import_dbf';
        $imp = new ImportDbf($this->conn);
        
        $imp
            ->fromFile($localFilename)
            ->setDbfCharset($charsetDbf)
            ->setCharset($charsetDb)
            //                ->setLimit(10)
            // Destination table
            ->setTable($tablename)
            //Ignore la premiere ligne
            ->setIgnoreFirstLine(false)
            // Prend la première ligne comme entête de colonnes
            ->setHeaderLikeFirstLine(true)
            ->setLimit(10)
            // Colonnes personnalisées
            //                            ->setFields($customFields)
            // Ajout de champs supplémentaires
            ->addFields(['calc_iduser', 'calc_ident'])
            // Ajout de n colonnes
            //            ->addFields(10)
            // Ajout d'un plugins
            ->addPlugins([$imp, 'pluginsNullValue'])
            // Ajout d'un plugins
            //                ->addPlugins(function ($rowData) {
            //                    $rowData['calcIduser'] = 'from plugins:' . $rowData['pkChantier'];
            //                    $rowData['calcIdent'] = 'from plugins:' . $rowData['uri'];
            //
            //                    return $rowData;
            //                })
            // required: Lecture/vérification
            ->read()
            // Exec import
            ->import();
        
        $this->assertEquals(10, count($imp->getRows()));
    }
    
    
    public function testImport3()
    {
        $this->createDb();
        
        
        $charsetDbf = 'cp863';
        $charsetDb = 'utf8_general_ci';
        
        $localFilename = __DIR__ . '/files/Agentes.DBF';
        $tablename = 'import_dbf';
        $imp = new ImportDbf($this->conn);
        
        $imp
            ->fromFile($localFilename)
            ->setDbfCharset($charsetDbf)
            ->setCharset($charsetDb)
            // Destination table
            ->setTable($tablename)
            //Ignore la premiere ligne
            ->setIgnoreFirstLine(false)
            // Prend la première ligne comme entête de colonnes
            ->setHeaderLikeFirstLine(false)
            ->setLimit(10)
            // Colonnes personnalisées
            //                            ->setFields($customFields)
            // Ajout de champs supplémentaires
            ->addFields(['calc_iduser', 'calc_ident'])
            // Ajout de n colonnes
            //            ->addFields(10)
            // Ajout d'un plugins
            ->addPlugins([$imp, 'pluginsNullValue'])
            // Ajout d'un plugins
            //                ->addPlugins(function ($rowData) {
            //                    $rowData['calcIduser'] = 'from plugins:' . $rowData['pkChantier'];
            //                    $rowData['calcIdent'] = 'from plugins:' . $rowData['uri'];
            //
            //                    return $rowData;
            //                })
            // required: Lecture/vérification
            ->read()
            // Exec import
            ->import();
        
        // 9 because the first line is use to the headers
        $this->assertEquals(10, count($imp->getRows()));
    }
}
